//
//  Services.swift
//  DesafioOsa
//
//  Created by Silvana Monasterios on 17-04-17.
//  Copyright © 2017 Silvana Monasterios. All rights reserved.
//

import UIKit
import Alamofire

class Services: NSObject {

    static func getPeliculas(search: String, pagina: Int ,completion: @escaping ([Pelicula], Int) ->Void){
        
        
        let parameters = ["s": search,
                          "page": pagina] as [String : Any]
        
        Alamofire.request("http://www.omdbapi.com/", method: .get, parameters: parameters).responseJSON { (response) in
            print("\(response)")
            
            if let resultado = response.result.value as? NSDictionary,
                let arreglo = resultado.object(forKey: "Search") as? [NSDictionary]{
                var arregloPelicula : [Pelicula] = []
                for item in arreglo{
                    arregloPelicula.append(Pelicula(dictionary: item))
                }
                
                var nroTitles : Int
                nroTitles = Int (resultado.object(forKey: "totalResults") as? String ?? "0") ?? 0
                var nroPages : Int = nroTitles / 10
                if nroTitles % 10 > 0{
                    nroPages = nroPages + 1
                }
                completion (arregloPelicula, nroPages)
            }
        }
        
    }
}
