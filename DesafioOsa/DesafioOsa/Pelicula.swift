//
//  Pelicula.swift
//  DesafioOsa
//
//  Created by Silvana Monasterios on 17-04-17.
//  Copyright © 2017 Silvana Monasterios. All rights reserved.
//

import UIKit

class Pelicula: NSObject {

    var title: String!
    var year : String!
    var type : String!
    var urlPoster: String!
    
    init(dictionary:NSDictionary) {
        title = dictionary.object(forKey: "Title") as? String ?? ""
        year = dictionary.object(forKey: "Year") as? String ?? ""
        type = dictionary.object(forKey: "Type") as? String ?? ""
        urlPoster = dictionary.object(forKey: "Poster") as? String ?? ""
    }
}
