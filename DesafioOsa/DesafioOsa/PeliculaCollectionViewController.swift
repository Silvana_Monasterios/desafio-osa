//
//  PeliculaCollectionViewController.swift
//  DesafioOsa
//
//  Created by Silvana Monasterios on 17-04-17.
//  Copyright © 2017 Silvana Monasterios. All rights reserved.
//

import UIKit

class PeliculaCollectionViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
     var listaPelicula : [Pelicula] = []
    var totalPages = 0
    var pagActual = 0
    var titlePelicula = ""
    var sizeItem = CGSize()
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var popUpImg: UIImageView!
    @IBOutlet weak var searchBarPeli: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Registrando celda pelicula xib
        collectionView.register(UINib.init(nibName: "PeliculasCollectionCell", bundle: nil), forCellWithReuseIdentifier: "peliculaCell")
        
        collectionView.register(UINib.init(nibName: "LoadingCollectionCell", bundle: nil), forCellWithReuseIdentifier: "loadingCell")
        
        //Coloca la separacion entre la celda del collectionView
        let flow = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flow.minimumLineSpacing = 30
        sizeItem = flow.itemSize
        
        
        cargaInicial(title: "Star Wars")
    }
    
    func cargaInicial (title : String){
        
        pagActual = 0
        totalPages = 1
        titlePelicula = title
        self.listaPelicula = []
        self.collectionView.reloadData()
        self.collectionView.contentOffset = CGPoint.zero
        downloadPage ()
    }
    
    //Hace la paginacion al servicio
    func downloadPage (){
        pagActual = pagActual + 1
        if pagActual <= totalPages{
            Services.getPeliculas(search: titlePelicula, pagina: pagActual) { (arrayPeliculas, paginas) in
                self.listaPelicula.append(contentsOf: arrayPeliculas)
                
                self.totalPages = paginas
                self.collectionView.reloadData()
            }
            
        }
    }

    //Metodos DataSource, Delegate Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return listaPelicula.count + (pagActual < totalPages ? 1 : 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if (indexPath.row == listaPelicula.count){
            return collectionView.dequeueReusableCell(withReuseIdentifier: "loadingCell", for: indexPath) as! LoadingCollectionCell
        }
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "peliculaCell", for: indexPath) as! PeliculasCollectionViewCell
        
        cell.configure(pelicula: listaPelicula[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if listaPelicula.count == indexPath.row + 1 {
            downloadPage ()
        }
        
    }
    
    //Añade gesto al popUpView para que se oculte al hacer click en el
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        popUpView.isHidden = false
        if let url = URL(string: listaPelicula[indexPath.row].urlPoster){
            popUpImg.kf.setImage(with: url, placeholder : UIImage (named: "placeHolderPosterPelicula"))
        }
        let gesture = UITapGestureRecognizer(target: self, action:#selector(closePopUpView))
        gesture.delegate = self
        popUpView.addGestureRecognizer(gesture)
    }
    
    //Oculta el popUpView
    func closePopUpView(){
        popUpView.isHidden = true
    }
    
    // Funcion que captura la orientación vertical o landscape al iphone y divide la cantidad de celda a aparecer en 2 o 3 columnas
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let orientation = UIApplication.shared.statusBarOrientation
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            //            UIEdgeInsete(<#T##top: CGFloat##CGFloat#>, <#T##left: CGFloat##CGFloat#>, <#T##bottom: CGFloat##CGFloat#>, <#T##right: CGFloat##CGFloat#>)sMak
            if(orientation == .landscapeLeft || orientation == .landscapeRight){
                //print("entro landscape")
                let auxItemWidth = sizeItem.width
                let newSpacing = (((view.bounds.width ) - ( auxItemWidth * 3)) ) / 4
                layout.minimumInteritemSpacing = newSpacing
                layout.sectionInset = UIEdgeInsetsMake(20,newSpacing , 20, newSpacing)
            }
            else{
                //print("entro vertical")
                let auxItemWidth = sizeItem.width
                let newSpacing = (((view.bounds.width ) - ( auxItemWidth * 2)) ) / 3
                layout.minimumInteritemSpacing = newSpacing
                layout.sectionInset = UIEdgeInsetsMake(20,newSpacing , 20, newSpacing)
                
            }
            
        }
        
    }
}

//Se valida para realizar la búsqueda de peliculas por su titulo
extension PeliculaCollectionViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == ""{
            cargaInicial(title: "Star Wars")
        }else{
            cargaInicial(title: searchText)
        }
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBarPeli.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBarPeli.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        view.endEditing(true)
        searchBar.text = ""
        cargaInicial(title: "Star Wars")
    }
    
}

//Agrega gesto a la imagen para que no oculte el popUpView si toca la imagen
extension PeliculaCollectionViewController: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (touch.view?.isDescendant(of: popUpImg)) ?? false{
            return false
        }
        return true
    }
}

