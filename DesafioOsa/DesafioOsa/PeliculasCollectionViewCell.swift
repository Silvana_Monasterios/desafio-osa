//
//  PeliculasCollectionViewCell.swift
//  DesafioOsa
//
//  Created by Silvana Monasterios on 17-04-17.
//  Copyright © 2017 Silvana Monasterios. All rights reserved.
//

import UIKit
import Kingfisher

class PeliculasCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgPelicula: UIImageView!
    @IBOutlet weak var titlePelicula: UILabel!
    
    func configure (pelicula: Pelicula){
        titlePelicula.text = pelicula.title
        
        if let url = URL(string: pelicula.urlPoster){
            imgPelicula.kf.setImage(with: url, placeholder : UIImage (named: "placeHolderPosterPelicula"))
        }
    }
}
